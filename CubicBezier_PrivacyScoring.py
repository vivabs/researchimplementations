# -*- coding: utf-8 -*-
"""
Created on Tue Dec 31 11:19:13 2013

@author: vidyalakshmi
"""

import numpy as np
import math
import time
from sklearn.cluster import KMeans,MiniBatchKMeans
from sklearn.datasets.samples_generator import make_blobs
import matplotlib.pyplot as plt
#from sklearn.metrics.pairwise import pairwise_distances_argmin
#from sklearn.metrics.pairwise import pairwise_distances_argmin



#Variables initialisation
No_of_Ego_Users = 1.0
#No_of_Friends = 200.0
No_of_Friends = 200
tx = No_of_Friends+1.0
ty = 50.0
l0 = 0.3
h0 = 0.7
lx = (1-l0)*tx
ly = ty*l0
hx = (1-h0)*tx
hy = ty*h0


def SquaresCubesAndRoots(num,action):
    if num < 0:
        if action == 'squarert':
           return -(math.sqrt(abs(num)))
        elif action == 'cubert':
           return -(math.pow(abs(num),1.0/3.0))
        elif action == 'cube':
           return -(math.pow(abs(num),3))
        elif action == 'square':
           return math.pow(abs(num),2)     
    elif num > 0:
        if action == 'squarert':
           return (math.sqrt(abs(num)))
        elif action == 'cubert':
           return (math.pow(abs(num),1.0/3.0))
        elif action == 'cube':
           return (math.pow(abs(num),3))
        elif action == 'square':
           return math.pow(abs(num),2)  
    else:
        return 1
    
## SquaresCubesAndRoots testing purpose only
#x1= SquaresCubesAndRoots(-1,'cubert')
#x2= SquaresCubesAndRoots(-10,'cube')
#x3= SquaresCubesAndRoots(-9,'squarert')
#x4= SquaresCubesAndRoots(-3,'square')
#
#x1= SquaresCubesAndRoots(1,'cubert')
#x2= SquaresCubesAndRoots(10,'cube')
#x3= SquaresCubesAndRoots(9,'squarert')
#x4= SquaresCubesAndRoots(3,'square')


Egousers = np.array([], dtype=str)
for users in range(0, int(No_of_Ego_Users)):
    Egousers = np.append(Egousers,'User'+ str(users))


#Assign friends with communication percentages and calculate Xn
Friends = np.array([[]], dtype=float)
for frnd in range(0, int(No_of_Friends)):
    s = np.random.uniform(0.10,.8,1)
    xval = (((frnd+1.0)/No_of_Friends))*((1-s))*((tx-1))
    #All from here for testing
    npSval = np.genfromtxt("D:/IMPORTANT/Prayogashaale/NannaPrayoga/Bezier Trials/Bezier_Results/Final/ReadIn_C.txt", dtype=float)
    s = npSval[frnd]
    xval = (((frnd+1.0)/No_of_Friends))*((1-npSval[frnd]))*(200)    
    #xval = frnd+1 #delete this sentence. Introduced for drawing figure only.
    
    # Till here
    TriTuple = np.array([[frnd,s,xval]]).reshape(1,3)
    xval = None
    if Friends.size == 0:
        Friends = TriTuple
    else: 
        Friends = np.append(Friends,TriTuple,axis=0)
#print Friends 
#    floatval = round(((frnd+1)/No_of_Friends),4)
#    print round(float(frnd+1/200),4)
#    print ((frnd+1)/No_of_Friends)
#    print (1-s)
#    print (tx-1)


#Cubic function derivation - Calcualte the f(x) which can be used in Y value calculation
 
Ca = ((3*lx)-(3*hx)+tx)
Cb = (3*hx)-(6*lx)
Cc = 3*lx
#Cd = 0

Combined = np.array([[]],dtype = float)
for eachfrnd in range(0,int(No_of_Friends)):
    Cd = -(Friends[eachfrnd,2])   #=-Xn
    Val = [Ca,Cb,Cc,Cd]
    ResultRoots = np.roots(Val)
    Real_val_Roots = ResultRoots.real[abs(ResultRoots.imag)<1e-5]
    Root = 800000000000000000000
    for rootnum in range(len(Real_val_Roots)):
        if Real_val_Roots[rootnum] > 0:
           Root = min(Root,Real_val_Roots[rootnum])
      
#    if ResultRoots.size == 0:
#        print 'empty'
#    if ResultRoots[0] > 0:
#        Root = min(Root,ResultRoots[0])
#    if ResultRoots[1] > 0:
#        Root = min(Root,ResultRoots[1])
#    if ResultRoots[2] > 0:
#        Root = min(Root,ResultRoots[2])
    try:
        if Combined.size == 0:
            XnPart = Root
            YnPrivScore = (((3*ly)-(3*hy)+ty)*(SquaresCubesAndRoots(XnPart,'cube')))+(((3*hy)-(6*ly))*(SquaresCubesAndRoots(XnPart,'square')))+(3*ly*XnPart)
            Combined = np.array([[XnPart,YnPrivScore]]).reshape(1,2)
            time.sleep(0.020)
        else:
            XnPart = Root
            YnPrivScore = (((3*ly)-(3*hy)+ty)*(SquaresCubesAndRoots(XnPart,'cube')))+(((3*hy)-(6*ly))*(SquaresCubesAndRoots(XnPart,'square')))+(3*ly*XnPart)
            Combined = np.append(Combined,(np.array([[XnPart,YnPrivScore]]).reshape(1,2)),axis=0)
            time.sleep(0.020)
    except ValueError:
        print ValueError
        XnPart = np.append(XnPart,0)

Friends = np.column_stack((Friends,Combined))   

#save to disk


